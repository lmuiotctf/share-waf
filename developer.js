
//-------------------------------------------------//
// ShareWAF - 二次开发模块
//-------------------------------------------------//

//提供了部分接口功能，用于调试、查看、记录、修改请求头、请求数据、返回数据等
//注：此模块权限低于ShareWAF原始功能，即若修改与ShareWAF冲突，则以ShareWAF为准

/**
 * 请求头修改
 * 参数：req_opt
 * 说明：请求在发送到被保护服务器之前，可在这里查看、记录、修改请求头内容
 */
exports.modify_req_option = function(req_opt){
    /**
     * 例程：
     * console.log(req_opt);
     * req_opt.headers["host"] = "www.sharewaf.com";
     */
    return req_opt;
}

/**
 * 请求数据修改
 * 参数：req_body
 * 说明：请求在发送到被保护服务器之前，可在这里查看、记录、修改请求数据，如Post内容等
 */
exports.modify_req_body = function(req_body){
    /**
     * 例程：
     * var req_body_str = req_body.toString();
     * console.log(req_body_str);
     * req_body = new Buffer.from(req_str);
     * console.log(req_body);
     */
    return req_body;
}

/**
 * 请求路径修改
 * 参数：req_url
 * 说明：请求在发送到被保护服务器之前，可在这里查看、记录、修改请求路径
 */
exports.modify_req_url = function(req_url){
    
    /**
     * 例程：
     * req_url = "/test.html"
     */
    return req_url;
}

/**
 * 返回数据头修改
 * 参数：res_header
 * 说明：返回数据发送给客户端之前，可在这里查看、记录、修改返回数据头
 */
exports.modify_res_header = function(res_header){
    
    /**
     * 示例：
     * res_header["Auther"] = "WangLiwen";
     */
    return res_header;
}

/**
 * 返回数据内容修改
 * 参数：
 *  proxyRes：返回头
 *  res_body：返回内容
 * 说明：返回数据发送给客户端之前，可在这里查看、记录、修改返回数据内容
 */
exports.modify_res_body = function(proxyRes,res_body){
    
    /**
     * 示例：
     * var res_body_str = res_body.toString() + "<br>ShareWAF developer<br>";
     * res_body = new Buffer.from(res_body_str);
     */

    /*
    //例：隐藏网页图片
    var content_type = proxyRes.headers["content-type"].toString().toLowerCase();
    if(content_type.indexOf("html") != -1){
        var hide_img = "";
        hide_img = hide_img + "<script>"
        hide_img = hide_img + "var img = document.getElementsByTagName('img');"
        hide_img = hide_img + "for(var i=0; i<img.length; i++){"
        hide_img = hide_img + "img[i].src='';"
        hide_img = hide_img + "}"
        hide_img = hide_img + "</script>"
        res_body = res_body + hide_img
    }
    */
    return res_body;
}

//-------------------------------------------------//
// 更多接口，尽请期待…
//-------------------------------------------------//