//配置内容需全小写
//本模块功能是否启用
exports.enable = 0;

//请求类型限制
exports.forbidden_request_type = [
    "got",
    "pest",
    "put",
    "head"
];
    
//协议头长度限制
exports.req_headers_key_length_limit = {
    "accept":128,
    "accept-Charset":256,
    "accept-Encoding":256,
    "cookie":1000,
    "referer":128,
    "user-agent":1000
};

//请求后缀过滤限制
exports.forbidden_request_extname = [
    ".zip",
    ".rar",
    ".exe",
    ".doc",
    ".php"
];

//url关键字限制
exports.url_fliter = [
    "中文",
    "nice",
]

//响应码限制
exports.forbidden_response_code = [
    201,
    405
];

//内容关键字限制
exports.content_fliter = [
    "shit",
    "fack"
]

//非法上传过滤
exports.upload_fliter = [
    ".asp",
    ".php",
    ".txt"
]

//https后台管理端口
exports.permit_https_port = 8090;

//磁盘、内存可用率告警值，0.01-1之间，0.01为1%；1为100%
exports.disk_warn_value = 0.00;
exports.memory_warn_value = 0.40;

//CPU使用率告警值，0.01-1之间，0.01为1%；1为100%
exports.cpu_warn_value = 0.00;

//强制以长连接请求
exports.keep_alive = 1;
exports.keep_alive_maxScokets = 55000;
exports.keep_alive_sockets = 55000;
exports.keep_alive_maxFreeSockets= 55000;
//毫秒
exports.keep_alive_keepAliveMsecs = 60000;
exports.keep_alive_keepAliveTimeout = 60000;
