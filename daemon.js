﻿/*
 * 主程序守护进程
 * 功能：检测主程序工作是否正常，如出现异常：无法访问，则对其进行重启
 * 本程序可以用forever启动，防止本进程出异常退出
 */
process.env.UV_THREADPOOL_SIZE = 128;

const { exec } = require('child_process');

/*
 * 启动
 */
function start_sharewaf(){
  exec('forever start sharewaf.js', (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
  });
}

/*
 * 关闭
 */
function stop_sharewaf(){
  exec('forever stop sharewaf.js', (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
  });
}

//启动
start_sharewaf();

var request = require('request');

//地址和端口
var sharewaf_host = "http://127.0.0.1:" + require('./config.js').shield_port + "/";
console.log("address:",sharewaf_host);

//10秒检测一次主程序服务是否正常
setInterval(function(){
  
  //访问
  request.get(sharewaf_host, {timeout: 5000}, function(err) {
    if (err != null){
      if(err.code == 'ETIMEDOUT' || err.code =='ECONNREFUSED' || err.code=='ESOCKETTIMEDOUT'){

        //重启
        stop_sharewaf();
        start_sharewaf();
      }else{
        console.log("Error:",err.code);
      }
    }
  }); 
}, 10000);
