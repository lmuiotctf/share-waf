exports.rules = [
    /*
     * 防护url中的攻击，检测url中出现的特定的内容，当match中条件全部成立时，视为规则成立
     * 可用于防SQL注入、XSS等
     * match_type类型：
     * indexof： 包含
     * notindexof：不包含
     * equel：等于
     * notequel：不等于
     */
    {
        id: "2006081743",
        description: "url防sql注入测试：'and 1 = 1",
        type: "url",
        enable: "1",
        match: [
            {
                match_type: "indexof",
                match_value: "'"
            },
            {
                match_type: "indexof",
                match_value: "and"
            },
            {
                match_type: "indexof",
                match_value: "1"
            }
        ]
    },
    {
        id: "2006081744",
        description: "url防sql注入：select from",
        type: "url",
        enable: "1",
        match: [
            {
                match_value: "select",
                match_type: "indexof"
            },
            {
                match_value: "from",
                match_type: "indexof"
            }
        ]
    },
    /*
     * 防护header内容中的攻击，检测指定请求头的内容，当match中条件全部成立时，视为规则成立
     * 可用于防扫描、防SQL注入、Cookie注入等
     * match_type类型：
     * indexof： 包含
     * notindexof：不包含
     * equel：等于
     * notequel：不等于
     */
    {
        id: "2006081746",
        description: "agent防护",
        type: "header",
        enable: "1",
        content: "user-agent",
        match: [
            {
                match_value: "Mozilla_123",
                match_type: "indexof"
            }
        ]
    },
    /*
     * 防护表格提交内容、文件上传中的攻击，当match中条件全部成立时，视为规则成立
     * 可用于防止非法文件上传、木马上传、SQL注入等
     * match_type类型：
     * indexof： 包含
     * notindexof：不包含
     */
    {
        id: "2006090826",
        description: "文件上传防护",
        type: "post_upload",
        enable: "1",
        match: [
            {
                match_value: ".js",
                match_type: "indexof"
            },
            {
                match_value: "username",
                match_type: "notindexof"
            }
        ]
    },
    /*
     * 访问防护，当match中任意条件成立时，视为规则成立
     * 可用于检测和防护机器人访问、CC、应用层DDOS、爬虫、批量注册、自动发贴
     * match规则说明：
     * page：被访问的页面，特定值：*，指任意页面
     * time_range：时间范围限制，单位为分钟，超过时间范围的话规则不生效
     * visit_times：访问计数
     */
    {
        id: "2006091914",
        description: "访问防护",
        type: "rule_mod_visitor",
        enable: "1",
        match: [
            {
                page: "*",
                time_range: "1",
                visit_times: "200"
            },
            {
                page: "/login.html",
                time_range: "1",
                visit_times: "100"
            }
        ]
    },
    /*
     * 风控，当match中全部条件成立时，视为规则成立
     * 可用于检测自动化攻击，如：撞库、应用层DDOS
     * match规则说明：
     * page：被访问的页面
     * time_range：时间范围限制，单位为分钟，超过时间范围的话规则不生效
     * visit_times：访问计数
     */
    {
        id: "2006091923",
        description: "风控规则",
        type: "risk_control",
        enable: "1",
        match: [
            {
                page: "/index.html",
                time_range: "1",
                visit_times: "5"
            },
            {
                page: "/login.html",
                time_range: "1",
                visit_times: "0"
            }
        ]
    }
]