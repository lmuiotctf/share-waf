exports.shield_port = 2019;
exports.router_port = 80;
exports.sa_port = 8080;
exports.ssl_on = 1;
exports.ssl_port = 443;
exports.cluster_enable = 0;
exports.use_sqlite3 = 1;
exports.skip_to = [{"domain":"www.fortest.com","target":"http://115.28.129.194/"},{"domain":"www.skipweb.com","target":"http://123.57.9.93/"}];
exports.debug = 0;
exports.use_mongodb = 0;
exports.mongodb_server = "mongodb://192.168.31.18:27017";
exports.shield_address = [["www.sharewaf.com","http://115.28.129.194/","",""],["www.jshaman.com","http://115.28.129.194/","",""]];
exports.js_protect_enable = [0,1];
exports.cache_enable = [1,0];
exports.cache_update_enable = [0,0];
exports.cache_file_type = [[".jpg",".png"],[""]];
exports.js_unprotect = [[""],[""]];
exports.transform_element_enable = [0,0];
exports.transform_element = [[""],["username"]];
exports.transform_element_type = [0,0];
exports.transform_element_cyc = ["3","3"];
exports.transform_element_once_at = ["3","3"];
exports.dynamic_token_enable = [0,0];
exports.dynamic_token_type = [0,0];
exports.dynamic_token = [[""],[""]];
exports.dynamic_token_once_at = ["1","1"];
exports.dynamic_token_cyc = ["1","1"];
exports.robot_detect_enable = [1,1];
exports.anti_ddos_enable = [1,0];
exports.browser_id_shield_enable = [0,0];
exports.regexp_shield_enable = [1,1];
exports.anti_scan_enable = [1,1];
exports.page_404_enable = [0,0];
exports.auto_transform = [0,0];
exports.auto_dynamic_token = [0,0];
exports.html_source_encode_enable = [1,1];
exports.html_source_encode_except = [[""],[""]];
exports.front_waf_auto = [1,1];
exports.front_waf_sql = [1,1];
exports.front_waf_xss = [1,1];
exports.front_waf_csrf = [1,1];
exports.front_waf_spider = [1,1];
exports.front_waf_iframe = [0,0];
exports.front_waf_no_copy = [0,0];
exports.front_waf_except = [[""],[""]];
exports.unknown = ["404","404"];
exports.eval_character = [[""],["'"]];
exports.black_ip_list = [[""],[""]];
exports.url_white_list = [[""],[""]];
exports.password = ["admin123","12345678"];
exports.page_coding = ["utf8","utf8"];
exports.page_404 = ["/404.html","/404.html"];
