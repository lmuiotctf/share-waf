/*
 * 校验码功能 开始
 */

var code;
//校验码
function createCode() 
{
	code = "";
	var codeLength = 4;
	var checkCode = document.getElementById("checkCode");
	var codeChars = new Array(3, 6, 2, 3, 4, 5, 6, 7, 8, 9, 
	'a','b','c','d','e','f','g','h','i','j','k','x','m','n','c','p','q','r','s','t','u','v','w','x','y','z',
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'K', 'M', 'N', 'b', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

	for(var i = 0; i < codeLength; i++) 
	{
		var charNum = Math.floor(Math.random() * 52);
		code += codeChars[charNum];
	}
	if(checkCode) 
	{
		checkCode.className = "code";
		checkCode.innerHTML = code;
	}
}

//判断校验码是否正确
function validateCode() 
{
	var inputCode=document.getElementById("inputCode").value;
	if(inputCode.length <= 0) 
	{
		alert("请输入验证码！");
		document.getElementById("inputCode").focus();
		return(false);
	}
	else if(inputCode.toUpperCase() != code.toUpperCase()) 
	{
		alert("验证码输入有误！");
		createCode();
		document.getElementById("inputCode").value="";
		document.getElementById("inputCode").focus();
		return(false);
	}
	else 
	{
		createCode();
		document.getElementById("inputCode").value="";
		submit_update();
		return(true);
	}    
}  

document.getElementById("update_config").onclick= function(){validateCode(); }

/*
 * 校验码功能 结束
 */

//连接服务器
sa_origins_host = window.location.protocol + "//" + window.location.host;
var socket = io(sa_origins_host);

//hook ajax open，捕捉错误
var hook_ajax_open = XMLHttpRequest.prototype.open;
XMLHttpRequest.prototype.open = function () {
	
	var xhrInstance = this; 
	xhrInstance.addEventListener("error", function (e) { 
		//alert("Error while connecting to ShareWAF server!");
		toastr.success('错误：无法连接到服务器!')
		return;
	});

	return hook_ajax_open.apply(this, arguments);        
}

 //登录成功
socket.on('login_success',function(data){
	swal({
		title: "",
		text: "登录成功！"
	});
	document.getElementById("div_login").style.display="none";
	document.getElementById("div_login_out").style.display="block";
	
	document.getElementById("div_config").style.display="block";
	document.getElementById("div_config_login_tip").style.display="none";

	document.getElementById("div_query").style.display="block";
	document.getElementById("div_query_login_tip").style.display="none";

	document.getElementById("div_sa").style.display="block";
	document.getElementById("div_sa_login_tip").style.display="none";

	document.getElementById("key_username").value = document.getElementById("login_username").value;
	document.getElementById("key_password").value = document.getElementById("login_password").value;

	document.getElementById("pem_username").value = document.getElementById("login_username").value;
	document.getElementById("pem_password").value = document.getElementById("login_password").value;

	document.getElementById("code_login_input").value = "";
	
	//登录后读取配置
	read_config();

	//刷新数据
	query_all();
	get_zxt();
	get_bxt();
	get_zzt();
	origens_query(0,'','');
})

//登录失败
socket.on('login_failed',function(data){
	document.getElementById("code_login_input").value = "";
	document.getElementById("verifyCanvas1").click();
	swal({
		title: "",
		text: "登录失败，帐号或密码错误！"
	});
})
socket.on('login_limit',function(data){
	document.getElementById("code_login_input").value = "";
	document.getElementById("verifyCanvas1").click();
	swal({
		title: "",
		text: "登录失败，帐号受限！"
	});
})
socket.on('login_vcode',function(data){
	document.getElementById("code_login_input").value = "";
	document.getElementById("verifyCanvas1").click();
	swal({
		title: "",
		text: "验证码已过期或错误。"
	});
})

//读配置文件返回
socket.on('read_config_return', function (data) {

	//去掉[、]、"符号
	data = data.replace(/\[/g,"");
	data = data.replace(/\]/g,"");
	data = data.replace(/\"/g,"");

	//数组化内容
	var read_config_content_arrary = data.split(";");

	$("#transform_element").importTags('');
	$("#js_unprotect").importTags('');
	$("#dynamic_token").importTags('');
	$("#eval_character").importTags('');
	$("#black_ip_list").importTags('');
	$("#url_white_list").importTags('');
	$("#html_source_encode_except").importTags('');
	$("#cache_file_type").importTags('');
	$("#front_waf_except").importTags('');

	//遍历成员
	read_config_content_arrary.forEach(function(c){

		//提取值
		var v =c.substr(c.indexOf("=")+1);
		//提取名称
		var n = c.substr(c.indexOf(".")+1,c.indexOf("=")-c.indexOf(".")-1);

		//去首尾空格
		v = v.trim();
		n = n.trim();
			
		//shield_address需要特殊处理
		if(n=="shield_address"){
			//转化为数组（这里未转化前都视为字符串了）
			var temp_arr = v.split(",");
			
			document.getElementById("shield_address_0").value = temp_arr[0];
			document.getElementById("shield_address_1").value = temp_arr[1];
			document.getElementById("shield_address_2").value = temp_arr[2];
			document.getElementById("shield_address_3").value = temp_arr[3];
		}

		if((n=="transform_element")||(n=="js_unprotect")||(n=="dynamic_token")||(n=="eval_character")||(n=="black_ip_list")||(n=="url_white_list")||(n=="html_source_encode_except")||(n=="front_waf_except")){
			//转化为数组（这里未转化前都视为字符串了）
			var temp_arr = v.split(",");
			//遍历各数组元素
			temp_arr.forEach(function(a){
				
				//防止重复添加
				if($("#"+n).val().toString().indexOf(a) ==-1){
					
					//添加到tag input中，这里用.value赋值是无效的，只能用下面这个方法
					//$("#"+n).importTags('');
					$("#"+n).addTag(a);
				}
			})
		}

		if(n=="cache_file_type"){
			//转化为数组（这里未转化前都视为字符串了）
			var temp_arr = v.split(",");
			//遍历各数组元素
			temp_arr.forEach(function(a){
				
				//防止重复添加
				if($("#"+n).val().toString().indexOf(a) ==-1){
					
					//添加到tag input中，这里用.value赋值是无效的，只能用下面这个方法
					//$("#"+n).importTags('');
					$("#"+n).addTag(a);
				}
			})
			return;
		}

		//定位元素
		var element = document.getElementById(n);
		
		if (element != undefined){
			//给元素赋值
			{
				element.value = v;
				
				if(v=="0"){
					element.checked = false;
				}else{
					element.checked = true;
				}
			}
			
			//判断radio元素，适用于变形元素类型和动态令牌类型
			if (n.indexOf("type")!=-1){
				if(v==1){
					document.getElementsByName(n)[0].checked = v;
				}else{
					console.log("......",n)
					document.getElementsByName(n)[1].checked = v;
					
				}
			}
		}
	
	});
});

//写配置文件成功
socket.on('write_config_ok',function(){
	swal({
		title: "",
		text: "配置已更新。"
	});	
});

//注册失败，已达最大域名数量
socket.on('register_faild_full',function(){
	swal({
		title: "注册失败",
		text: "服务器已达最大保护域名数量。"
	});	
});

//注册失败，已达最大域名数量
socket.on('register_failed_vcode',function(){
	swal({
		title: "注册失败",
		text: "验证码已过期或错误。"
	});	
});

//注册失败，域名已经存在
socket.on('register_faild_exist',function(){
	swal({
		title: "注册失败",
		text: "域名已经存在。"
	});	
});

//注册成功
socket.on('register_success',function(){
	swal({
		title: "",
		text: "注册成功。"
	});	
	//关闭注册窗口
	document.getElementById("close_modal").click();
});


//清空缓存成功
socket.on('clear_cache_success',function(){
	swal({
		title: "",
		text: "清空缓存成功。"
	});	
	
});
//清空缓存失败
socket.on('clear_cache_error',function(){
	swal({
		title: "",
		text: "清空缓存失败。"
	});	
});

//上传文件返回信息
socket.on('upload_return',function(data){
	swal({
		title: "",
		text: data.msg
	});	

	var upload_file = data.file;
	if(upload_file.indexOf("key")!=-1){
		document.getElementById('shield_address_2').value = upload_file;
		return;
	}
	if(upload_file.indexOf("pem")!=-1){
		document.getElementById('shield_address_3').value = upload_file;
		return;
	}
});

//查询数据返回
socket.on('query_data_return', function (data) {
	console.log("query_data_return",data);
	document.getElementById("total_"+data.val).innerHTML = data.cou;
});
socket.on('query_data_range_return', function (data) {
	console.log("query_data_range_return",data);

	switch(data.val){
		case "visitor":
			visitor_count = data.cou;
			visitor_refresh = 1;
			break;
		case "sqlinj_or_xss_attack":
			sqlinj_or_xss_attack_count = data.cou;
			sqlinj_or_xss_attack_refresh = 1;
			break;
		case "robot_attack":
			robot_attack_count =data.cou;
			robot_attack_refresh = 1
			break;
		case "exceeds_authorized_access":
			exceeds_authorized_access_count = data.cou;
			exceeds_authorized_access_refresh = 1;
			break;
		case "stealing_ling":
			stealing_ling_count = data.cou;
			stealing_ling_refresh = 1;
			break;
		case "regexp_shield":
			regexp_shield_count = data.cou;
			regexp_shield_refresh = 1;
			break;
		case "unknown":
			unknown_count = data.cou
			unknown_refresh = 1;
			break;
	}

	pie_refresh();
	zzt_refresh();

});

socket.on('show_trend_return', function (data) {

	var barOptions = {
        series: {
            lines: {
                show: true,
                lineWidth: 2,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.0
                    }, {
                        opacity: 0.0
                    }]
                }
            }
        },
        xaxis: {
            tickDecimals: 0
        },
        colors: ["#1ab394"],
        grid: {
            color: "#999999",
            hoverable: true,
            clickable: true,
            tickColor: "#D4D4D4",
            borderWidth:0
        },
        legend: {
            show: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "x: %x, y: %y"
        }
    };
	
	var visitor_data = {
		label: "visitor",
		data: []
	};

	var start = document.getElementById("zxt_start").value;
	var end = document.getElementById("zxt_end").value;
	
	var temp_start;
	temp_start=start;
	temp_start=temp_start.split("/");
	start = temp_start[2]+temp_start[0]+temp_start[1];

	var temp_end;
	temp_end = end;
	temp_end=temp_end.split("/");
	end = temp_end[2]+temp_end[0]+temp_end[1];

	var d=[];
	var v=[];
	var temp_d;
	var index;
	for(i=0;i<data.length;i++){

		temp_d = data[i].time.toString().substr(0,8);
		
		index = d.toString().indexOf(temp_d);
		if(index==-1){
			d.push(temp_d);
			v.push(1);
		}else{
			v[index] = v[index] +1;
		}
	}

	for(k=0;k<d.length;k++){
		visitor_data.data.push([d[k],v[k]]);
	}
	visitor_data.label = "DATA COUNT:" + data.length;

	$.plot($("#flot-line-chart"), [visitor_data],barOptions);
});

var visitor_count = 0;
var sqlinj_or_xss_attack_count = 0;
var robot_attack_count = 0;
var exceeds_authorized_access_count = 0;
var stealing_ling_count = 0;
var regexp_shield_count = 0;
var unknown_count = 0;

var visitor_refresh = 0;
var sqlinj_or_xss_attack_refresh = 0;
var robot_attack_refresh = 0;
var exceeds_authorized_access_refresh = 0;
var stealing_ling_refresh = 0;
var regexp_shield_refresh = 0;
var unknown_refresh = 0;

socket.on('show_visitor_return', function (data) {
	visitor_count = data;
	visitor_refresh = 1;
	pie_refresh();
	zzt_refresh();
});
socket.on('show_sqlinj_or_xss_attack_return', function (data) {
	sqlinj_or_xss_attack_count = data;
	sqlinj_or_xss_attack_refresh = 1;
	pie_refresh();
	zzt_refresh();
});
socket.on('show_robot_attack_return', function (data) {
	robot_attack_count = data;
	robot_attack_refresh = 1;
	pie_refresh();
	zzt_refresh();
});
socket.on('show_exceeds_authorized_access_return', function (data) {
	exceeds_authorized_access_count = data;
	exceeds_authorized_access_refresh = 1;
	pie_refresh();
	zzt_refresh();
});
socket.on('show_stealing_ling_return', function (data) {
	stealing_ling_count = data;
	stealing_ling_refresh = 1;
	pie_refresh();
	zzt_refresh();
});
socket.on('show_regexp_shield_return', function (data) {
	console.log("show_regexp_shield_return",data);
	regexp_shield_count = data;
	regexp_shield_refresh = 1;
	pie_refresh();
	zzt_refresh();
});
socket.on('show_unknown_return', function (data) {
	unknown_count_count = data;
	unknown_refresh = 1;
	pie_refresh();
	zzt_refresh();
});

//饼型图更新
function pie_refresh(){
	//如果有一项数据没有返回，则先退出
	if((unknown_refresh==0)||(stealing_ling_refresh==0)||(exceeds_authorized_access_refresh==0)||(robot_attack_refresh==0)||(sqlinj_or_xss_attack_refresh==0)||(regexp_shield_refresh==0)||(visitor_refresh==0)){
		return;
	}
	var data = [{
        label: "正常访问:"+visitor_count,
        data: visitor_count,
        color: "#d3d3d3",
    }, {
        label: "SQL注入和XSS:"+sqlinj_or_xss_attack_count,
        data: sqlinj_or_xss_attack_count,
        color: "#bababa",
    }, {
        label: "机器人攻击:"+robot_attack_count,
        data: robot_attack_count,
        color: "#79d2c0",
    }, {
        label: "越权:"+exceeds_authorized_access_count,
        data: exceeds_authorized_access_count,
        color: "#1ab394",
	}, {
        label: "盗链:"+stealing_ling_count,
        data: stealing_ling_count,
        color: "#1ab394",
    }, {
        label: "正则表达式:"+regexp_shield_count,
        data: regexp_shield_count,
        color: "#1ab394",
    }, {
        label: "未知攻击:"+unknown_count,
        data: unknown_count,
        color: "#1ab394",
    }];

    var plotObj = $.plot($("#flot-pie-chart"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });
}

//柱状图更新
function zzt_refresh(){
	var barOptions = {
        series: {
            bars: {
                show: true,
                barWidth: 0.6,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.8
                    }, {
                        opacity: 0.8
                    }]
                }
            }
        },
        xaxis: {
            tickDecimals: 0
        },
        colors: ["#1ab394"],
        grid: {
            color: "#999999",
            hoverable: true,
            clickable: true,
            tickColor: "#D4D4D4",
            borderWidth:0
        },
        legend: {
            show: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "x: %x, y: %y"
        }
    };
    var barData = {
		label: "1.正常访问:"+visitor_count,
		data: [
			[1, visitor_count],
		]
	};
		
	var barData2 = {
		label: "2.SQL注入和XSS:"+sqlinj_or_xss_attack_count,
		data: [
			[2, sqlinj_or_xss_attack_count],
		]
	};
	var barData3 = {
		label: "3.机器人攻击:"+robot_attack_count,
		data: [
			[3, robot_attack_count],
		]
	}
	var barData4 = {
		label: "4.越权:"+exceeds_authorized_access_count,
		data: [
			[4, exceeds_authorized_access_count],
		]
	}
	var barData5 = {
		label: "5.盗链:"+stealing_ling_count,
		data: [
			[5, stealing_ling_count],
		]
	};
	var barData6 = {
		label: "6.正则表达式:"+regexp_shield_count,
		data: [
			[6, regexp_shield_count],
		]
	};
	var barData7 = {
		label: "7.未知攻击:"+unknown_count,
		data: [
			[7, unknown_count],
		]
	};

    $.plot($("#flot-bar-chart"), [barData,barData2,barData3,barData4,barData5,barData6,barData7], barOptions);
}

 /*
  * 登录 开始
  */
function login(){
	var username = document.getElementById("login_username").value;
	var password = document.getElementById("login_password").value;
	if (username.length == 0) {
		swal({
			title: "",
			text: "帐号不能为空。"
		});
		//document.getElementById("login_username").focus();
		return;
	}
	if (password.length == 0) {
		swal({
			title: "",
			text: "密码不能为空。"
		});
		//document.getElementById("login_username").focus();
		return;
	}
	if ((username.indexOf(".") == -1) || (username.length<5)){
		swal({
			title: "",
			text: "帐号有误，请检查。"
		});
		//document.getElementById("login_username").focus();
		return;
	}

	if(socket.connected == true){
		//发送登录请求
		socket.emit('login',username,password,document.getElementById("code_login_input").value);
	}else{
		alert("未连接到服务器。请检查配置或向客服咨询。");
	}

}
  /*
  * 登录 结束
  */

  /*
   * 注册 开始 
   */
function register(){
	var register_username = document.getElementById("register_username").value;
	var register_target = document.getElementById("register_target").value;
	var register_password = document.getElementById("register_password").value;
	var register_confirm = document.getElementById("register_confirm").value;

	if (register_username.length == 0) {
		swal({
			title: "",
			text: "帐号不能为空。"
		});
		return;
	}

	if ((register_username.indexOf(".") == -1) || (register_username.length<5)){
		swal({
			title: "",
			text: "帐号有误，请检查。"
		});
		//document.getElementById("register_username").focus();
		return;
	}

	if (register_target.length == 0) {
		swal({
			title: "",
			text: "保护目标不能为空。"
		});
		return;
	}


	if ((register_target.indexOf(".") == -1) || (register_target.length<5)){
		swal({
			title: "",
			text: "保护目标有误，请检查。"
		});
		document.getElementById("register_target").focus();
		return;
	}

	if (register_target.toLowerCase().indexOf("http") == -1){
		swal({
			title: "",
			text: "保护目标需以http://或https://起始。"
		});
		document.getElementById("register_target").focus();
		return;
	}

	if (register_password.length == 0) {
		swal({
			title: "",
			text: "密码不能为空。"
		});
		return;
	}

	if (register_password.length <= 5) {
		swal({
			title: "",
			text: "密码太短了吧。"
		});
		return;
	}

	if (register_password != register_confirm) {
		swal({
			title: "",
			text: "两次输入的密码不一致。"
		});
		return;
	}


	//发送注册请求
	if(socket.connected == true){
		socket.emit('register', register_username, register_password,register_target,document.getElementById("code_register_input").value);
	}else{
		alert("未连接到服务器。请检查配置或向客服咨询。");
	}
}
   /*
   * 注册 结束
   */

   /*
	* 读取配置 开始 
    */
   	function read_config(){
		socket.emit('read_config',document.getElementById("login_username").value);
	}
   /*
	*读取配置 结束 
    */
   
/**
 * 更新配置 开始
 */
//提交更新内容
function submit_update(){

	var data ={};
	var temp = document.getElementById("shield_address_0").value + "," + document.getElementById("shield_address_1").value + "," + document.getElementById("shield_address_2").value + "," + document.getElementById("shield_address_3").value;
	data.shield_address = temp.split(",");
	data.js_protect_enable = document.getElementById("js_protect_enable").checked?1:0;
	data.cache_enable = document.getElementById("cache_enable").checked?1:0;
	data.cache_update_enable = document.getElementById("cache_update_enable").checked?1:0;
	data.js_unprotect = document.getElementById("js_unprotect").value.split(",");
	data.transform_element_enable = document.getElementById("transform_element_enable").checked?1:0;
	data.transform_element = document.getElementById("transform_element").value.split(",") ;
	data.transform_element_type = document.getElementsByName("transform_element_type")[0].checked?1:0 ;
	data.transform_element_cyc = document.getElementById("transform_element_cyc").value;
	data.transform_element_once_at = document.getElementById("transform_element_once_at").value;
	data.dynamic_token_enable = document.getElementById("dynamic_token_enable").checked?1:0;
	data.dynamic_token_type = document.getElementsByName("dynamic_token_type")[0].checked?1:0;
	data.dynamic_token =document.getElementById("dynamic_token").value.split(",");
	data.dynamic_token_once_at = document.getElementById("dynamic_token_once_at").value;
	data.dynamic_token_cyc = document.getElementById("dynamic_token_cyc").value;
	data.robot_detect_enable = document.getElementById("robot_detect_enable").checked?1:0;
	data.anti_ddos_enable = document.getElementById("anti_ddos_enable").checked?1:0;
	data.browser_id_shield_enable = document.getElementById("browser_id_shield_enable").checked?1:0;
	data.regexp_shield_enable = document.getElementById("regexp_shield_enable").checked?1:0;
	data.anti_scan_enable = document.getElementById("anti_scan_enable").checked?1:0;
	data.page_404_enable = document.getElementById("page_404_enable").checked?1:0;
	data.auto_transform = document.getElementById("auto_transform").checked?1:0;
	data.auto_dynamic_token = document.getElementById("auto_dynamic_token").checked?1:0;
	data.html_source_encode_enable = document.getElementById("html_source_encode_enable").checked?1:0;
	data.unknown = document.getElementById("unknown").value;
	data.eval_character = document.getElementById("eval_character").value.split(",");
	data.black_ip_list = document.getElementById("black_ip_list").value.split(",");
	data.url_white_list = document.getElementById("url_white_list").value.split(",");
	data.password = document.getElementById("password").value;
	data.page_coding = document.getElementById("page_coding").value;
	data.page_404 = document.getElementById("page_404").value;
	data.front_waf_except = document.getElementById("front_waf_except").value.split(",");
	data.html_source_encode_except = document.getElementById("html_source_encode_except").value.split(",");
	data.cache_file_type = document.getElementById("cache_file_type").value.split(",");
	data.front_waf_auto = document.getElementById("front_waf_auto").checked?1:0;
	data.front_waf_sql = document.getElementById("front_waf_sql").checked?1:0;
	data.front_waf_xss = document.getElementById("front_waf_xss").checked?1:0;
	data.front_waf_csrf = document.getElementById("front_waf_csrf").checked?1:0;
	data.front_waf_spider = document.getElementById("front_waf_spider").checked?1:0;
	data.front_waf_iframe = document.getElementById("front_waf_iframe").checked?1:0;
	data.front_waf_no_copy = document.getElementById("front_waf_no_copy").checked?1:0;
	console.log(data);
	//向服务器发送内容
	socket.emit('write_config',data,document.getElementById("login_username").value, document.getElementById("login_password").value);
}
/**
 * 更新配置 结束
 */

/**
 * 查询各类型数据总量 开始
 */
function query_data(type, value){
	var domain = document.getElementById("login_username").value.toString();
	socket.emit('query_data', { domain:domain, type:type, value:value});
}

function query_all(){
	query_data('type','visitor');
	query_data('type','sqlinj_or_xss_attack');
	query_data('type','robot_attack');
	query_data('type','exceeds_authorized_access');
	query_data('type','stealing_ling');
	query_data('type','regexp_shield');
	query_data('type','unknown');
}
//query_all();
/**
 * 查询各类型数据总量 结束
 */


 /**
  * 攻击详细列表 开始
  */
	
var origins_query_page = 1;
var origins_query_page_max =1;

//禁用或启用按钮
function button_disabled(b, value){
	document.getElementById(b).disabled = value;
}

//根据返回数量情况，对上下页按钮可用状态进行调整
function button_status(){

	//前后页的状态，都先设为可用
	button_disabled("origens_previous",false);
	button_disabled("origens_next",false);

	button_disabled("search_origens_previous",false);
	button_disabled("search_origens_next",false);

	//到了最前页，则向前按钮不可用
	if (origins_query_page == 1){
		button_disabled("origens_previous",true);
		button_disabled("search_origens_previous",true);
	}
	//到了最后页，则向后按钮不可用
	if (origins_query_page == origins_query_page_max){
		button_disabled("origens_next",true);
		button_disabled("search_origens_next",true);
	}

	if(search_flag==""){
		button_disabled("search_origens_previous",true);
		button_disabled("search_origens_next",true);
	}else{
		button_disabled("origens_previous",true);
		button_disabled("origens_next",true);
	}
}
button_disabled("origens_previous",true);
button_disabled("origens_next",true);
button_disabled("search_origens_previous",true);
button_disabled("search_origens_next",true);

var search_flag;
//查询攻击详细列表
function origens_query(c,search_type,search_value){

	//如果未登录，返回
	if(document.getElementById("div_query_login_tip").style.display != "none") {
		return;
	}

	var start = document.getElementById("gjsj_start").value;
	var end = document.getElementById("gjsj_end").value;
	
	var temp_start;
	temp_start=start;
	temp_start=temp_start.split("/");
	start = temp_start[2]+temp_start[0]+temp_start[1];

	var temp_end;
	temp_end = end;
	temp_end=temp_end.split("/");
	end = temp_end[2]+temp_end[0]+temp_end[1];

	start=start + "000000";
	end = end + "240000";

	var days = (temp_end[2] - temp_start[2]) * 365 + (temp_end[0] - temp_start[0]) * 30 + (temp_end[1] - temp_start[1])
	if(days<0){
		alert("选择有误！请重新选择。");
		return;
	}
	if((temp_end[2] - temp_start[2])>=1){
		alert("抱歉，只可查询一个年度内的数据。请重新选择。");
		return;
	}
	
	console.log ("查询",days,"天的数据。",start,end);
	var start_time = start;
	var end_time = end;

	console.log(c,search_type,search_value);
	for(i=0;i<10;i++){
		
		document.getElementById("NO_"+i).innerText  = "0-" + i;
		document.getElementById("time_"+i).innerText  = "Time";
		document.getElementById("ip_"+i).innerText  = "ip";
		document.getElementById("agent_"+i).innerText  = "Agent";
		document.getElementById("jwd_"+i).innerText  = "Lon_Lat";
		document.getElementById("attack_type_"+i).innerText  = "Attack_Type";
		document.getElementById("url_"+i).innerText  = "URL";
	}

	if (c == "pre"){
		origins_query_page = origins_query_page - 1;
		if (origins_query_page<1){
			origins_query_page = 1;
			
		}	
	}else if( c=="nex" ){
		origins_query_page = origins_query_page + 1;
		if(origins_query_page >= origins_query_page_max){
			origins_query_page = origins_query_page_max;
			
		}
	}else if( c==undefined){
		origins_query_page = 1;
	}

	//搜索标识，给前后翻页使用
	search_flag = search_type;

	//发起查询，每页10条数据
	socket.emit('origins_query', { start:origins_query_page, count:10, domain:document.getElementById("login_username").value, search_type:search_type, search_value:search_value, start_time:start_time, end_time:end_time});
	console.log("start:",origins_query_page,"count:",10,"search_type:",search_type,"search_value:",search_value);
}

//查询返回
socket.on('origins_query_return', function (data) {
	
	console.log(data);
	for(i=0;i<data.rows.length;i++){
		
		document.getElementById("NO_"+i).innerText  = origins_query_page + "-" + i;
		document.getElementById("time_"+i).innerText  = data.rows[i].time ;
		document.getElementById("ip_"+i).innerText  = data.rows[i].ip;
		document.getElementById("agent_"+i).innerText  = data.rows[i].agent;
		document.getElementById("jwd_"+i).innerText  = data.rows[i].lon_lat;
		document.getElementById("attack_type_"+i).innerText  = data.rows[i].type;
		document.getElementById("url_"+i).innerText  = data.rows[i].url;
	}

	//总页数和当前页数
	origins_query_page_max = parseInt(data.rows_length/10)+1;
	console.log(origins_query_page_max);
	if(origins_query_page_max<=0){origins_query_page_max=1;}
	document.getElementById("origins_query_info").innerHTML = origins_query_page + "/" + origins_query_page_max;
	document.getElementById("origins_query_count").innerHTML = data.rows_length;
	
	button_status();
});
/**
  * 攻击详细列表 结束
  */