﻿/*
 * 反csrf攻击
 * CSRF = Cross-site request forgery，跨站请求伪造
 * 实现方法：
 * 给cookie增加SameSite属性，Strict为严格模式、Lax为宽松模式
 */

/*
 * 如果页面中原本有window.onload事件，则先进行调用，以免影响原有的代码功能，下同
 */
var pre_window_load=window.onload;
if (pre_window_load!=undefined){
    pre_window_load();
}

/*
 * 页面加载完成后执行
 */
window.onload=function(){
    console.log("anti_crsf module loaded");
    var pre_cookie = document.cookie.toLowerCase();
    if(pre_cookie.indexOf("samesite")==-1){
        document.cookie = pre_cookie + ";SameSite=Strict;";
    }
}
