/*
 * 反爬虫
 * 实现方法：
 * 清空href内容，使链接地址不可见，点击时再还原，可链接生效，且操作起来跟之前无差别
 */

/*
 * 如果页面中原本有window.onload事件，则先进行调用，以免影响原有的代码功能，下同
 */
var pre_window_load=window.onload;
if (pre_window_load!=undefined){
    pre_window_load();
}

//全局变量，存储全部链接的href
var pre_href=[];

/*
 * 页面加载完成后执行
 */
window.onload=function(){
    console.log("anti_spider module loaded");

    //拦截第三方库驱动的访问
    var user_agent = navigator.userAgent.toLowerCase();
    if( (user_agent.indexOf("phantomjs") != -1) || (user_agent.indexOf("selenium") != -1) || (user_agent.indexOf("casperjs") != -1) ){
        document.body.innerHTML="";
    }

    //清空链接的href，使爬虫无法获取链接
    var link = document.getElementsByTagName("a");
    for(var i=0; i<link.length; i++){

        pre_href[i] = link[i].href;
        link[i].href="";

        //获取之前的onfocus事件
        var pre_onfocus = link[i].onfocus;

        //注册click事件处理程序，即onfocus
        link[i].addEventListener("focus",function(){

            restore_href(this)
            //如果之前有onfocus事件处理程序，则先执行
            if(pre_onfocus!=undefined){
                pre_onfocus;
            }
            
        });

    }
};

/*
 * 还原href，使链接可打开
 */
function restore_href(t){
    var href = document.getElementsByTagName('a');
    for(var i=0; i<href.length; i++){
        if(href[i]==t){
            href[i].href=pre_href[i];
        }
    }
}
