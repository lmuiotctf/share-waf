/*
 * 防iframe嵌套
 */

/*
 * 如果页面中原本有window.onload事件，则先进行调用，以免影响原有的代码功能，下同
 */
var pre_window_load=window.onload;
if (pre_window_load!=undefined){
    pre_window_load();
}

/*
 * 页面加载完成后执行
 */
window.onload=function(){
    console.log("anti_iframe module loaded")
    if(top.location != self.location){
		top.location = self.location;
	}
};