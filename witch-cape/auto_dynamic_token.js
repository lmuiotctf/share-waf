﻿
//令牌功能
function sharewaf_token(){
    
     //当前时间
     var time_now = new Date().getTime().toString();

    var sharewaf_token = get_sharewaf_token();
    var sharewaf_token_ecc = get_sharewaf_token_ecc();
    
    if( sharewaf_token == null ){
        //没有令牌

        if( window.location.href.indexOf("?") != -1 ){
            window.location.href=window.location.href + "&sharewaf_token="+time_now + "&sharewaf_token_ecc=" + Secret_Key("sharewaf_token_ecc",time_now,"encryption");
        }else{
            window.location.href=window.location.href + "?sharewaf_token="+time_now + "&sharewaf_token_ecc=" + Secret_Key("sharewaf_token_ecc",time_now,"encryption");
        }

    }else{
        //有令牌

        //当前时间
        var new_time = new Date().getTime().toString();

        //更新令牌
        if(( new_time - sharewaf_token) > 1000){
            
            var new_url;
            //新令牌
            new_url  = window.location.href.replace(sharewaf_token,new_time);
            //新令牌校验码
            new_url  = new_url.replace(sharewaf_token_ecc,Secret_Key("sharewaf_token_ecc",new_time,"encryption"));

            //重定向
            window.location.href = new_url;
        }
    }

}

var pre_window_load=window.onload;
if (pre_window_load!=undefined){
    pre_window_load();
}
window.onload=function(){
    sharewaf_token();
    console.log("auto_dynamic_token module loaded");
};

//获取自动动态令牌
function get_sharewaf_token()
{
    var reg = new RegExp("(^|&)"+ "sharewaf_token" +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);

    if(r!=null)
        return  unescape(r[2]); 

    return null;
}
//获取自动动态令牌校验码
function get_sharewaf_token_ecc()
{
    var reg = new RegExp("(^|&)"+ "sharewaf_token_ecc" +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);

    if(r!=null)
        return  unescape(r[2]); 

    return null;
}

