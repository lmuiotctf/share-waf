
/*
 * 反自动化攻击
 * 
 * 现实方法：
 * 1、调用本模块时，自动设置页面所有password控件的id、name为空，这样可防止自动化工具或脚本根据id、name填写内容，只有当点击时才还原正常的原始id、name；
 * 2、自动化工具或脚本也可能模拟点击键入内容。为防止这种操作，接管input事件，监听输入行为，如果检测到一次性输入多个字符，则其极可能为自动填写的，这时清空password框内容使输无效
 * 3、防xpath定位元素方式的自动填写内容
 */

/*
 * 如果页面中原本有window.onload事件，则先进行调用，以免影响原有的代码功能，下同
 */
var pre_window_load=window.onload;
if (pre_window_load!=undefined){
    pre_window_load();
}

//全局变量，存储页面全部password控件的id、name
var input_id=[], input_name=[];

/*
 * 页面加载完成后执行
 */
window.onload=function(){
console.log("anti_automated_attack module loaded");
    //随机插入input，防止xpath根据input顺序获取password框    
    var rand_num = (Math.floor(Math.random()*3));
    for(i=0;i<rand_num ;i++){
        var rand_div = document.createElement("input");
        rand_div.setAttribute("type","hidden");

        var first_input = document.getElementsByTagName("input")[0];
        if(first_input!=undefined){
            //节点的父节点插入，参数：新元素，节点
            first_input.parentElement.insertBefore(rand_div,first_input);
        }
    }

    //清空password控件id和name，使无法分析页面元素，无法自动化攻击
    var input = document.getElementsByTagName("input");
    
    for(var i=0; i<input.length; i++){

        if(input[i].type=="password"){
            input_id[i] = input[i].id;
            input_name[i] = input[i].name;
            input[i].id="";
            input[i].name="";

            //获取之前的onchange事件
            var pre_onchange = input[i].onchange;

            //注册change事件处理程序，即onchange
            input[i].addEventListener("change",function(){

                //如果之前有onchange事件处理程序，则先执行
                if(pre_onchange!=undefined){
                    pre_onchange;
                }
                restore_id_name(this)
            });

            //获取之前的oninput事件
            var pre_oninput = input[i].oninput;
            //注册input事件处理程序，即oninput
            input[i].addEventListener("input",function(){

                //如果之前有oninput事件处理程序，则先执行
                if (pre_oninput != undefined){
                    pre_oninput;
                }
                var input_ret = detect_input(this);
                if (input_ret==false){
                    this.value="";
                }
            });
        }
    }
};

/*
 * 当password框发生点击或失去焦点时，表明可能是人为操作，则还原存储的ID和Name，使可像保护前一样正常提交数据
 */
function restore_id_name(t){
    var input = document.getElementsByTagName('input');
    for(var i=0; i<input.length; i++){
        if(input[i]==t){
            input[i].id=input_id[i];
            input[i].name=input_name[i];
        }
    }
}

/*
 * 检测输入，如果一次变化长度超过1，说明不是手工输入的，极可能是模拟按键类软件的操作
 * 注：也可能是复制粘贴操作，但这里的功能禁止密码框粘贴操作
 */
var input_password_len = 0;
function detect_input(t){
    console.log("detect_input",t.value);

    if(t.value.length - input_password_len>1){
        console.log("Please input password one by one.")
        return false;
    }
    input_password_len =t.value.length;
    return true;
}