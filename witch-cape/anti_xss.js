
/*
 * 反XSS攻击
 * 实现方法：
 * 替换输入框中的xss特征字符
 */

/*
 * 如果页面中原本有window.onload事件，则先进行调用，以免影响原有的代码功能，下同
 */
var pre_window_load=window.onload;
if (pre_window_load!=undefined){
    pre_window_load();
}

/*
 * 页面加载完成后执行
 */
window.onload=function(){
    console.log("anti_xss module loaded");

    //判断input输入框中没有没xss语句
    var input = document.getElementsByTagName("input");
    for(var i=0; i<input.length; i++){

        if (input[i].type != "password"){
            //获取之前的onblur事件
            var pre_blur = input[i].onblur;

            //注册blur事件处理程序，即onblur
            input[i].addEventListener("blur",function(){

                //如果之前有oncblur事件处理程序，则先执行
                if(pre_blur!=undefined){
                    pre_blur;
                }
                detect_input_xss(this);
            });
        }
    }
    
    //判断textarea输入框中没有没xss语句
    var textarea = document.getElementsByTagName("textarea");
    for(var i=0; i<textarea.length; i++){

        //获取之前的onblur事件
        var pre_blur = textarea[i].onblur;

        //注册blur事件处理程序，即onblur
        textarea[i].addEventListener("blur",function(){

            //如果之前有oncblur事件处理程序，则先执行
            if(pre_blur!=undefined){
                pre_blur;
            }
            detect_input_xss(this);
        });
           
    }
}

function detect_input_xss(t){
    //技巧：判断包含"和'的方法
    if(t.value.indexOf("<")!=-1 || t.value.indexOf(">")!=-1 || t.value.indexOf("'")!=-1 || t.value.indexOf('"')!=-1  ){
        console.log("change xss character");
        t.value = no_xss(t.value);
    }
}

//转码
function no_xss(s){
    return s.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g, "&quot;").replace(/'/g, "&#039;");
}